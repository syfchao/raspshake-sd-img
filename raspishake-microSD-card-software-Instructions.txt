Rasberry Shake micro-SD card Software Installation Procedure

The Raspberry Shake software image burning procedure has been greatly simplified with a new release format.
Instead of having to use a special program to "burn" the "image", the only thing now required
is to open the .zip file and extract its contents to the micro-SD card.

Complete instructions are as follows:

1. Confirm your micro-SD card is formatted to FAT32, cards usually come from the manufacturer pre-formatted.
   1.1 Format Utilities: https://howtorecover.me/best-programs-usb-flash-drives-formatting
   1.2 Use one of these to format your SD card, to guarantee all available space is, actually, available
   1.3 Confirm your SD card has at least 7GB of free space

2. Download raspishake-release.zip to your computer.

3. Unzip raspishake-release.zip, extracting the contents to the micro-SD card
  3.1  This should take between 60 and 90 seconds

4. Insert the micro-SD card into the Raspberry Shake's Raspberry Pi computer and power up the device
  4.1. The Raspberry Pi computer will begin the Raspberry Shake software image unpacking task automatically

5. Wait until the unpacking task completes and your Raspberry Shake has re-booted
  5.1.  Depending on the card size and type, this will take between 10 and 17 minutes
  5.2.  During this process the LED lights will display B-solid on the Raspberry Shake board, and 
        R&G-solid on the Raspberry Pi computer
  5.3.  When completed, the LED's will be B-solid on the Raspberry hake board, with R-solid and 
        G-blinking on the Raspberry Pi computer
  5.2.  As well, you will know the system has booted when you are able to find an IP address on the 
        network belonging to the Raspberry Shake.  For example, either via your router interface, 
        or using a program like Fing
        
6.  And from all of us here at Shake Central, Enjoy your Raspberry Shake!


Burning the Old Way
Found inside the zip file is the image formatted the same way as for previous releases.
By extracting it specifically, you are able to burn this image to disk using any 
known program (e.g., etcher), or the 'dd' command from the command line.

