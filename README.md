Ver 18.4


<pre><code>&nbsp;&nbsp;FILE: raspishake-release.zip   
MD5SUM: 2be2008a4283555b3986b6723c29c317
</code></pre>

[<img src="https://gitlab.com/raspberryShake-public/raspShake-SD-img/raw/master/_static/dl_button.png" width="400" alt="Click to Download Raspberry Shake OS" title="Click to Download Raspberry Shake OS">](https://gitlab.com/raspberryShake-public/raspShake-SD-img/raw/master/raspishake-release.zip)

Instructions to burn the image to your SD card are [here](https://gitlab.com/raspberryShake-public/raspShake-SD-img/blob/master/raspishake-microSD-card-software-Instructions.txt)

Changelog can be found [here](https://manual.raspberryshake.org/changelog.html#changelog)

- All models of Raspberry Shake, including:
  - Raspberry Shake [1D, 3D, 4D], 
  - Raspberry Boom,
  - Raspberry Shake-n-Boom, and 
  - Raspberry Jam 
  use the same SD card image.

- Includes suppot for 3 Model B and B+
- Includes support for 4 Model B

We strongly recommend against using consumer-grade TLC microSD cards with Raspberry Shake. 
Use commercial-grade (MLC) or industrial-grade (SLC) microSD cards instead to avoid unnecessary SD card corruptions.
